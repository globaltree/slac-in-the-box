;;;;; SLACIFY ;;;;

;; Installs the Slackware port to Aarch64 (arm64)
;; onto encrypted logical volumes customized
;; for the pinebook pro


(let (
      (
       (path-to-slarm64-current (progn
		       (format t "~&~&Enter the Absolute Pathname to the top level of the slarm64-current tree):  ~&~&~&~&")
		       (read-line)))
       (targetdevice (progn
		       (format t "~&~&Enter the Target Device (/dev/yourdevicename):  ~&~&~&~&")
		       (read-line)))
       (targethostname (progn
			 (format t "~&~&Give the target device a hostname:  ~&~&~&~&)")
			 (read-line)))
       (passkey (progn
		  (format t "~&~&Create a passkey to be entered upon every boot:  ~&~&~&~&")
		  (read-line)))
       (targetswapsize (progn
		  (format t "~&~&Size of target swap partion, in GB (leave off the unit, and enter an integer:  ~&~&~&~&")
		  (read-line)))))
       
  (progn
    
    ;; store passkey in temporary ramfs
    (let (
	  (create-mountpoint (format nil "mkdir -p /mnt/ramfs"))
	  (mount-ramfs (format nil "mount -t ramfs -o size=64k,maxsize=64k ramfs /mnt/ramfs")))
      (progn
	(inferior-shell:run/i create-mountpoint)
	(inferior-shell:run/i mount-ramfs)
	(with-open-file (f /mnt/tmpfs/keyfile
			   :direction :output
			   :if-exists :supersede
			   :if-doesn-not-exist :create)
			((format nil "~a" passkey) s f))))
    
  ;; partiton device
    (let (
	  (wipefs (format nil "wipefs -a ~a" targetdevice))
	  (parted (format nil "parted -s ~a mklabel gpt mkpart boot ext4 32768s 262143s mkpart rootfs 262144s 100% set 1 boot on set 2 lvm on" targetdevice)))
      (progn
	(inferior-shell:run/i wipefs)
	(inferior-shell:run/i parted)))

    ;; fill lvm partition with random data
    (let (
	  (fill-with-random (format nil "dd if=/dev/urandom of=~ap2 bs=512 status=progress" targetdevice)))
      (inferior-shell:run/i fill-with-random))
    

	  ;; format luks container
    (let (
	(luksformat (format nil "cryptsetup luksFormat -s 256 -y --key-file keyfile ~ap2" targetdevice)))
	(inferior-shell:run/i luksformat))

    ;; open luks container
    (let (
	  (luksopen (format nil "cryptsetup luksOpen ~ap2 ~azcrypt" targetdevice targethostname)))
      (inferior-shell:run/i luksopen))

    ;; configure lvm
    (let (
	  (pvcreate (format nil "pvcreate /dev/mapper/~azcrypt" targethostname))
	  (vgcreate (format nil "vgcreate ~azvg /dev/mapper/~azcrypt" targethostname targethostname))
	  (create-swap-volume (format nil "lvcreate -L ~aG -n swap ~azvg" targetswapsize targethostname))
	  (create-root-volume (format nil "lvcreate -L 50%FREE -n root ~azvg" targethostname))
	  (create-home-volume (format nil "lvcreate -L 100%FREE -n home ~azvg" targethostname))
	  (mkswap (format nil "mkswap -L ~azswap /dev/~azvg/swap" targethostname targethostname)))
      (progn
	(inferior-shell:run/i pvcreate)
        (inferior-shell:run/i vgcreate)
	(inferior-shell:run/i create-swap-volume)
	(inferior-shell:run/i create-root-volume)
	(inferior-shell:run/i create-home-volume)
	(inferior-shell:run/i mkswap)))

    ;; activate logical volumes
    (let (
	  (activate-lvm (format nil "vgchange -ay")))
      (inferior-shell:run/i activate-lvm))

    ;; create file systems
    (let (
	  (format-boot (format nil "mkfs.ext4 -O 64bit -L ~azboot ~ap1" targetdevice))
	  (format-root (format nil "mkfs.ext4 -O 64bit -L ~azroot /dev/~azvg/root" targethostname targethostname))
	  (format-home (format nil "mkfs.ext4 -O 64bit -L ~azhome /dev/~azvg/home" targethostname targethostname)))
      (progn
	(inferior-shell:run/i format-boot)
	(inferior-shell:run/i format-root)
	(inferior-shell:run/i format-home)))

    ;; mount target file systems
    (let (
	  (make-target-mountpoint (format nil "mkdir -p /mnt/~a" targethostname))
	  (mount-root-fs (format nil "mount -t ext4 -L ~azroot /mnt/~a" targethostname targethostname))
	  (make-boot-mountpoint (format nil "mkdir -p /mnt/~a/boot" targethostname))
	  (make-home-mountpoint (format nil "mkdir -p /mnt/~a/home" targethostname))
	  (mount-boot-fs (format nil "mount -t ext4 -L ~azboot /mnt/~a/boot" targethostname targethostname))
	  (mount-home-fs (format nil "mount -t ext4 -L ~azhome /mnt/~a/home" targethostname targethostname)))
      (progn
	(inferior-shell:run/i make-target-mountpoint)
	(inferior-shell:run/i make-root-fs)
	(inferior-shell:run/i make-boot-mountpoint)
	(inferior-shell:run/i make-home-mountpoint)
	(inferior-shell:run/i mount-boot-fs)
	(inferior-shell:run/i mount-home-fs)))

    ;; ensure slarm64-current is up-to-date
    (let (
	  (rsync-slarm64-current (format nil "rsync -Pruv --delete --exclude="extra/aspell*" --exclude="pasture/*" --exclude="source/*" --exclude="testing/*" --exclude="kde/*" --exclude="kdei/*" --progress  dl.slarm64.org::slarm64/slarm64-current/ ~a" path-to-slarm64-current)))
      (inferior-shell:run/i rsync-slarm64-current))

    ;; install slarm64 to target
    (let (
	  (slacify (format nil "for package in $(ls slarm64/*/*.t?z); do installpkg --root /mnt/~a $package && sync; done;" targethostname)))
      (inferior-shell:run/i slacify))
    ;; configure slarm64 on target
    (progn
    ;; set hostname
    (with-open-file (f (format nil "/mnt/~a/etc/HOSTNAME" targethostname)
		       :direction :output
		       :if-exists :supersede
		       :if-doesn-not-exist :create)
		    ((format nil "~a.~a" targethostname targetdomain) s f))
    ;; create file system table
    (with-open-file (f (format nil "/mnt/~a/etc/fstab" targethostname)
			   :direction :output
			   :if-exists :supersede
			   :if-doesn-not-exist :create)
			((format nil "#/etc/fstab~&\
                                      LABEL=~azroot          /                    ext4          defaults              1         1~&\
                                      LABEL=~azboot          /boot                ext4          defaults              1         0~&\
                                      LABEL=~azhome          /home                ext4          defaults              1         2~&\
                                      LABEL=~azswap          none                 swap          defaults              0         0~&\
                                      devpts                 /dev/pts             devpts        gid=5,mode=620        0         0~&\
                                      proc                   /proc                proc          defaults              0         0~&\
                                      tmpfs                  /dev/shm             tmpfs         defaults              0         0" targethostname targethostname targethostname targethostname ) s f))
    ;; create /etc/hosts
    (with-open-file (f (format nil "/mnt/~a/etc/hosts" targethostname)
			   :direction :output
			   :if-exists :supersede
			   :if-doesn-not-exist :create)
			((format nil "#/etc/hosts ~&\
                                      127.0.0.1          localhost~&\
                                      127.0.0.1          ~a.~a               ~a" targethostname targetdomain targethostname) s f))

    ;; set timezone
    (let (
	  (remove-old-localtime (format nil "rm -f /mnt/~a/etc/localtime" targethostname))
	  (link-new-localtime (format nil "cd /mnt/~a/etc && ln -s ../usr/share/zoneinfo/America/Los_Angeles && cd ~" targethostname)))
      (inferior-shell:run/i remove-old-localtime)
      (inferior-shell:run/i link-new-localtime))

    ;; configure ntp
    (with-open-file (f (format nil "/mnt/~a/etc/ntp.conf" targethostname)
			   :direction :output
			   :if-exists :append
			   :if-doesn-not-exist :create)
		    ((format nil "server 2.pool.ntp.org iburst~&server 3.pool.ntp.org iburst") s f))

    ;; bind mount system for chrooting
    (let (
	  (bind-dev (format nil "mount -o bind /dev /mnt/~a/dev" targethostname))
	  (bind-proc (format nil "mount -o bind /proc /mnt/~a/proc" targethostname))
	  (bind-sys (format nil "mount -o bind /sys /mnt/~a/sys" targethostname)))
      (inferior-shell:run/i bind-dev)
      (inferior-shell:run/i bind-proc)
      (inferior-shell:run/i bind-sys))

    ;; chroot and set a root password
    (let (
	  (chpasswd (format nil "chpasswd -R /mnt/~a" targethostname)))
      (inferior-shell:run/i chpasswd))

    ;; chroot and build a kernel
    ;; chroot and build an initrd
    ;; configure /boot/extlinux/extlinux.conf
    
