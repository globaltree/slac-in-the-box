;;;;; gnu linux kernel builder for pinebook pro

;;; by Everett Fuller, AKA slac.in.the.box

;;; this software is protected by magic:
;;; https://www.protected.by.magic.lol

;;; this program builds a kernel from source code found in /usr/src/linux
;;; make /usr/src/linux a symbolic link to /usr/src/linux-version-to-build
;;; this program will look for config-pinebook-pro in the same directory from
;;; which the program is executed, and if not present will attempt to download
;;; it from the https://gitlab.com/globaltree/slac.in.the.box repository

;;; the program also builds kerne firmware, kernel headers, kernel modules, and a device-tree-blob for
;;; the pinebook pro..  It then packages everything into a slackware package to install
;;; with slackware's pkgtools.


;;; load lisp systems used  by this program:
(ql:quickload "inferior-shell")

;;; get a copy of config-pinebook-pro to /usr/src
(let (
      (get-config (format nil "wget https://gitlab.com/globaltree/slac.in.the.box/config-pinebook-pro"))
      (mv-config (format nil "mv config-pinebook-pro /usr/src/")))
  (inferior-shell:run/i get-config)
  (inferior-shell:run/i mv-config))

;;; change directories to the kernel source code directory and clean it
(let (
      (clean-source (format nil "ARCH=arm64 cd /usr/src/linux && make mrproper && make distclean")))
  (inferior-shell:run/i clean-source))

;;; cp /usr/src/config-pinebook-pro to /usr/src/linux/.config
(let (
      (copy-config (format nil "cp /usr/src/config-pinebook-pro /usr/src/linux/.config")))
  (inferior-shell:run/i copy-config))

;;; make oldconfig
(let (
      (make-oldconfig (format nil "ARCH=arm64 make -j$(nproc) oldconfig")))
  (inferior-shell:run/i make-oldconfig))

;;; make kernel, kernel modules, and device tree blob.
(let (
      (build-kernel (format nil "ARCH=arm64 make -j$(nproc) kernel"))
      (build-kernel-modules (format nil "ARCH=arm64 make -j$(nproc) modules"))
      (build-device-tree-blob (format nil "ARCH=arm64 make -j$(nproc) dtbs")))
  (inferior-shell:run/i build-kernel)
  (inferior-shell:run/i build-kernel-modules)
  (inferior-shell:run/i build-device-tree-blob))

;;; download and build kernel firmware
;;; add supplemental pinebook pro firmware
;;; make target directory from which to build slackware pwackage
;;; populate target directory with target subdirectories
;;; use cat >> to copy kernel to target.  Copy kernel modules and
;;; device tree blob to target directory
;;; write a slack-desc file
;;; write a doinst.sh file, that builds an initrd after kernel is installed
;;; make a slackware package








